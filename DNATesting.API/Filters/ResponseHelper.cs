﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.ModelBinding;
using DNATesting.Helper;

namespace DNATesting.Api.Filters
{
    public class ResponseHelper
    {
        public static HttpResponseMessage InternalServerError(HttpRequestMessage request, string exceptionMessage)
        {
            return request.CreateErrorResponse(HttpStatusCode.InternalServerError, MessageConstants.ServerError, new Exception(exceptionMessage));
        }
        public static HttpResponseMessage OK(HttpRequestMessage request, object data)
        {
            return request.CreateResponse(HttpStatusCode.OK, data);
        }
        public static HttpResponseMessage NotFound(HttpRequestMessage request, string exceptionMessage)
        {
            return request.CreateErrorResponse(HttpStatusCode.NotFound, exceptionMessage, new Exception(exceptionMessage));
        }
        public static HttpResponseMessage Forbidden(HttpRequestMessage request)
        {
            return request.CreateErrorResponse(HttpStatusCode.Forbidden, MessageConstants.AccessTokenFailed, new Exception(MessageConstants.AccessTokenFailed));
        }
        public static HttpResponseMessage Unauthorized(HttpRequestMessage request)
        {
            return request.CreateErrorResponse(HttpStatusCode.Unauthorized, MessageConstants.AuthrizationFailed, new Exception(MessageConstants.AuthrizationFailed));
        }
        public static HttpResponseMessage BadRequest(HttpRequestMessage request, ModelStateDictionary modelState)
        {
            return request.CreateErrorResponse(HttpStatusCode.BadRequest, modelState);
        }
        public static HttpResponseMessage MethodNotAllowed(HttpRequestMessage request)
        {
            return request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, MessageConstants.InvalidCredentials, new Exception(MessageConstants.InvalidCredentials));
        }
        public static HttpResponseMessage AlreadyExists(HttpRequestMessage request, string type)
        {
            return request.CreateErrorResponse(HttpStatusCode.Conflict, type, new Exception(MessageConstants.AlreadyExists.Replace("{type}", type)));
        }
        public static HttpResponseMessage MethodNotAllowed(HttpRequestMessage request, string message)
        {
            return request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, message, new Exception(message));
        }
    }
}