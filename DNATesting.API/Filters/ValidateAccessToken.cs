﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Helper;

namespace DNATesting.Api.Filters
{
    public class ValidateAccessTokenAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                HttpRequestHeaders header = actionContext.Request.Headers;
                if (header.Contains("Authorization") == false)
                    actionContext.Response = ResponseHelper.Forbidden(actionContext.Request);
                else
                {
                    IEnumerable<string> x_access_token = header.GetValues("Authorization");
                    if (x_access_token == null && string.IsNullOrEmpty(x_access_token.FirstOrDefault()) && string.IsNullOrWhiteSpace(x_access_token.FirstOrDefault()))
                        actionContext.Response = ResponseHelper.Forbidden(actionContext.Request);

                    int? userId = Token.ValidateToken(x_access_token.FirstOrDefault());

                    UserService authService = new UserService();
                    UserDTO objUser = authService.GetUser(userId);
                    
                    if (objUser == null)
                    {
                        actionContext.Response = ResponseHelper.Unauthorized(actionContext.Request);
                    }
                    else
                    {
                        KeyValuePair<string, object> userKey = new KeyValuePair<string, object>();
                        userKey = new KeyValuePair<string, object>("userId", objUser.Id);
                        actionContext.Request.Properties.Add(userKey);
                    }
                }
            }
            catch (Exception ex)
            {
                actionContext.Response = ResponseHelper.InternalServerError(actionContext.Request, MessageConstants.ServerError);
            }
        }
    }
}