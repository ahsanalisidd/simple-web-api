﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DNATesting.Api.Controllers.Api
{
    public class BaseController : ApiController
    {
        //private readonly LoggingService _loggingService;
        public int LoggedInUserId
        {
            get
            {
                return (int)Request.Properties["userId"];
            }
        }

        public BaseController()
        {
            //_loggingService = new LoggingService();
        }

        protected void Log(string description, string formName)
        {
            //LoggingDTO logging = new LoggingDTO();
            //logging.LogDescription = description;
            //logging.FormName = formName;
            //logging.CreatedBy = LoggedInUserId;

            //_loggingService.Log(logging);
        }
    }
}
