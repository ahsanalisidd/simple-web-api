﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using DNATesting.Api.Controllers.Api;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;

namespace DNATesting.Api.Controllers.api
{
    [ValidateAccessToken]
    public class DashboardController : BaseController
    {
        private readonly DashboardService _dashboardService;
        public DashboardController()
        {
            _dashboardService = new DashboardService();
        }

        [HttpPost]
        public IHttpActionResult Post(DashboardSearchDTO obj)
        {
            try
            {
                return Ok(_dashboardService.GetCounts(obj.FromDate,obj.ToDate));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetCounts");
                return BadRequest(ex.Message);
            }
        }
    }
}