﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Customer;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]

    [RoutePrefix("api/Customer")]
    public class CustomerController : BaseController
    {
        private readonly CustomerService _customerService;
        public CustomerController()
        {
            _customerService = new CustomerService();
        }

        [Route("GetByPage")]
        [HttpGet]
        public IHttpActionResult GetByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_customerService.GetRoles(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetCustomer");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingle")]
        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            try
            {
                return Ok(_customerService.GetSingle(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetCustomer");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(_customerService.GetAll());
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetCustomer");
                return BadRequest(ex.Message);
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult Create(CustomerDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_customerService.Add(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveCustomer");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetCustomerReport")]
        [HttpPost]
        public IHttpActionResult GetCustomerReport(CustomerReportDTO obj)
        {
            try
            {
                return Ok(_customerService.GetCustomerReport(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveCustomer");
                return BadRequest(ex.Message);
            }
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update(CustomerDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_customerService.Edit(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveCustomer");
                return BadRequest(ex.Message);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                CustomerDTO obj = new CustomerDTO();
                obj.Id = id;
                return Ok(_customerService.Delete(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteCustomer");
                return BadRequest(ex.Message);
            }
        }
    }
}