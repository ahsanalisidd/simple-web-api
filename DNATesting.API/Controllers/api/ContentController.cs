﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.DataLayer;
using DNATesting.Domain.Model.Content;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    [RoutePrefix("api/Content")]
    public class ContentController : BaseController
    {
        private readonly IContentService _contentService;
        public ContentController()
        {
            _contentService = new ContentService();
        }

        [Route("GetByPage")]
        [HttpGet]
        public IHttpActionResult GetByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_contentService.GetRoles(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetContent");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingle")]
        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            try
            {
                return Ok(_contentService.GetSingle(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetContent");
                return BadRequest(ex.Message);
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult Create(ContentDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_contentService.Add(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveContent");
                return BadRequest(ex.Message);
            }
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update(ContentDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_contentService.Edit(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveContent");
                return BadRequest(ex.Message);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                ContentDTO obj = new ContentDTO();
                obj.Id = id;
                return Ok(_contentService.Delete(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteContent");
                return BadRequest(ex.Message);
            }
        }
    }
}