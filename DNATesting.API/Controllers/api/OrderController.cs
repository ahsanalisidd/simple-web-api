﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.DataLayer;
using DNATesting.Domain.Model.Order;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    [RoutePrefix("api/Order")]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        public OrderController()
        {
            _orderService = new OrderService();
        }

        [Route("GetByPage")]
        [HttpGet]
        public IHttpActionResult GetByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_orderService.GetRoles(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRoles");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingle")]
        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            try
            {
                return Ok(_orderService.GetSingle(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRight");
                return BadRequest(ex.Message);
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult Create(OrderDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_orderService.Add(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveOrder");
                return BadRequest(ex.Message);
            }
        }


        [Route("GetOrderReport")]
        [HttpPost]
        public IHttpActionResult GetOrderReport(OrderReportDTO obj)
        {
            try
            {
                return Ok(_orderService.GetOrderReport(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveCustomer");
                return BadRequest(ex.Message);
            }
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update(OrderDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_orderService.Edit(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "UpdateOrder");
                return BadRequest(ex.Message);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                OrderDTO Right = new OrderDTO();
                Right.Id = id;
                return Ok(_orderService.Delete(Right));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteOrder");
                return BadRequest(ex.Message);
            }
        }
    }
}