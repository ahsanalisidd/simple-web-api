﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    public class RoleController : BaseController
    {
        private readonly RoleService _roleService;
        public RoleController()
        {
            _roleService = new RoleService();
        }

        [HttpGet]
        public IHttpActionResult Get(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_roleService.GetRoles(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRoles");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_roleService.GetRole(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRole");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Post(RoleDTO role)
        {
            try
            {
                return Ok(_roleService.SaveRole(role, base.LoggedInUserId));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveRole");
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                RoleDTO role = new RoleDTO();
                role.Id = id;
                return Ok(_roleService.DeleteRole(role, base.LoggedInUserId));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteRole");
                return BadRequest(ex.Message);
            }
        }
    }
}