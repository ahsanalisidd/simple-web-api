﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Model;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    public class UserController : BaseController
    {
        private readonly UserService _userService;
        public UserController()
        {
            _userService = new UserService();
        }

        [HttpGet]
        public IHttpActionResult Get(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_userService.GetUsers(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetUsers");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_userService.GetUser(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetUser");
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public IHttpActionResult Post(UserDTO user)
        {
            try
            {
                return Ok(_userService.SaveUser(user, base.LoggedInUserId));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveUser");
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UserDTO user = new UserDTO();
                user.Id = id;
                return Ok(_userService.DeleteUser(user, base.LoggedInUserId));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteUser");
                return BadRequest(ex.Message);
            }
        }
    }
}
