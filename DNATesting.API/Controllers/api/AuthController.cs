﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;

namespace DNATesting.Api.Controllers.Api
{
    public class AuthController : ApiController
    {
        private readonly AuthService _authService;
        public AuthController()
        {
            _authService = new AuthService();
        }

        [HttpPost]
        public IHttpActionResult Login(LoginDTO loginModel)
        {
            try
            {
                AuthResponseDTO response = new AuthResponseDTO();
                bool isValid = _authService.Login(loginModel.Username, loginModel.Password, ref response);
                if (isValid)
                {
                    return Ok(response);
                }
                else
                {
                    return Ok(false);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
