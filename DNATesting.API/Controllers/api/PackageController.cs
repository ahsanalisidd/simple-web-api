﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.DataLayer;
using DNATesting.Domain.Model.package;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    [RoutePrefix("api/Package")]
    public class PackageController : BaseController
    {
        private readonly IPackageService _packageService;
        public PackageController()
        {
            _packageService = new PackageService();
        }

        [Route("GetByPage")]
        [HttpGet]
        public IHttpActionResult GetByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_packageService.GetPackage(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRoles");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(_packageService.GetAll());
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRoles");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingle")]
        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            try
            {
                return Ok(_packageService.GetSingle(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRight");
                return BadRequest(ex.Message);
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult Create(PackageDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_packageService.Add(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SavePackage");
                return BadRequest(ex.Message);
            }
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update(PackageDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_packageService.Edit(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "UpdatePackage");
                return BadRequest(ex.Message);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                PackageDTO Right = new PackageDTO();
                Right.Id = id;
                return Ok(_packageService.Delete(Right));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeletePackage");
                return BadRequest(ex.Message);
            }
        }
    }
}