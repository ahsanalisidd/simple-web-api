﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.DataLayer;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    [RoutePrefix("api/PackageType")]
    public class PackageTypeController : BaseController
    {
        private readonly IPackageTypeService _packageTypeService;
        public PackageTypeController()
        {
            _packageTypeService = new PackageTypeService();
        }

        [Route("GetByPage")]
        [HttpGet]
        public IHttpActionResult GetByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_packageTypeService.GetRoles(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRoles");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingle")]
        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            try
            {
                return Ok(_packageTypeService.GetSingle(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRight");
                return BadRequest(ex.Message);
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult Create(PackageTypeDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_packageTypeService.Add(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveRight");
                return BadRequest(ex.Message);
            }
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update(PackageTypeDTO right)
        {
            try
            {
                return Ok(_packageTypeService.Edit(right));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveRight");
                return BadRequest(ex.Message);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                PackageTypeDTO Right = new PackageTypeDTO();
                Right.Id = id;
                return Ok(_packageTypeService.Delete(Right));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteRight");
                return BadRequest(ex.Message);
            }
        }
    }
}