﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.DataLayer;
using DNATesting.Domain.Model.Coupon;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    [RoutePrefix("api/Coupon")]
    public class CouponController : BaseController
    {
        private readonly ICouponService _orderTypeService;
        public CouponController()
        {
            _orderTypeService = new CouponService();
        }

        [Route("GetByPage")]
        [HttpGet]
        public IHttpActionResult GetByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_orderTypeService.GetRoles(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetOrderType");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingle")]
        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            try
            {
                return Ok(_orderTypeService.GetSingle(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetCoupon");
                return BadRequest(ex.Message);
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult Create(CouponDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_orderTypeService.Add(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveCoupon");
                return BadRequest(ex.Message);
            }
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update(CouponDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_orderTypeService.Edit(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveCoupon");
                return BadRequest(ex.Message);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                CouponDTO obj = new CouponDTO();
                obj.Id = id;
                return Ok(_orderTypeService.Delete(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteCoupon");
                return BadRequest(ex.Message);
            }
        }
    }
}