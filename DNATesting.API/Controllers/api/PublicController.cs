﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Customer;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.Domain.Model.Public;
using DNATesting.Domain.Model.Order;

namespace DNATesting.Api.Controllers.Api
{
    [RoutePrefix("api/Public")]
    public class PublicController : BaseController
    {
        private readonly IPublicService _publicService;
        public PublicController()
        {
            _publicService = new PublicService();
        }

        [Route("SingUp")]
        [HttpPost]
        public IHttpActionResult SingUp(SignUpDTO obj)
        {
            try
            {
                return Ok(_publicService.SignUp(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SignUp");
                return BadRequest(ex.Message);
            }
        }

        [Route("UpdateCustomer")]
        [HttpPost]
        public IHttpActionResult UpdateCustomer(PubCustomerDTO obj)
        {
            try
            {
                return Ok(_publicService.EditCustomer(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SignUp");
                return BadRequest(ex.Message);
            }
        }

        [Route("SingIn")]
        [HttpPost]
        public IHttpActionResult SingIn(SignInDTO obj)
        {
            try
            {
                var res = _publicService.SignIn(obj);
                if (res != null)
                    return Ok(res);
                else
                    return BadRequest("Error: Invalid user email or password");
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SignIn");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetAllPackage")]
        [HttpGet]
        public IHttpActionResult GetAllPackage()
        {
            try
            {
                return Ok(_publicService.GetPubPackage());
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SignIn");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetPackageByPage")]
        [HttpGet]
        public IHttpActionResult GetPackageByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_publicService.GetPubPackageByPage(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SignIn");
                return BadRequest(ex.Message);
            }
        }

        [Route("VerifyAccount")]
        [HttpPost]
        public IHttpActionResult VerifyAccount(AccountVerifyDTO obj)
        {
            try
            {
                return Ok(_publicService.VerifyAccount(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "VerifyAccount");
                return BadRequest(ex.Message);
            }
        }

        [Route("RecentVerificationEmail")]
        [HttpGet]
        public IHttpActionResult RecentVerificationEmail(string email)
        {
            try
            {
                return Ok(_publicService.VerificationByEmail(email));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "VerifyAccount");
                return BadRequest(ex.Message);
            }
        }

        [Route("ForgotPassword")]
        [HttpGet]
        public IHttpActionResult ForgotPassword(string email)
        {
            try
            {
                return Ok(_publicService.ForgotPassword(email));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "VerifyAccount");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingleOrder")]
        [HttpGet]
        public IHttpActionResult GetSingleOrder(int id)
        {
            try
            {
                return Ok(_publicService.GetSingleOrder(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SingleOrder");
                return BadRequest(ex.Message);
            }
        }

        [Route("CreateOrder")]
        [HttpPost]
        public IHttpActionResult CreateOrder(OrderDTO obj)
        {
            try
            {
                return Ok(_publicService.CreateOrder(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "CreateOrder");
                return BadRequest(ex.Message);
            }
        }

        [Route("CancelOrder")]
        [HttpPut]
        public IHttpActionResult CancelOrder(int Id)
        {
            try
            {
                return Ok(_publicService.CancelOrder(Id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "CancelOrder");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetAllOrders")]
        [HttpGet]
        public IHttpActionResult GetAllOrders()
        {
            try
            {
                return Ok(_publicService.GetAllOrder());
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "AllOrder");
                return BadRequest(ex.Message);
            }
        }

        [Route("UpdatePassword")]
        [HttpPost]
        public IHttpActionResult UpdatePassword(ForgotPasswordDTO obj)
        {
            try
            {
                return Ok(_publicService.UpdatePassword(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "VerifyAccount");
                return BadRequest(ex.Message);
            }
        }

    }
}