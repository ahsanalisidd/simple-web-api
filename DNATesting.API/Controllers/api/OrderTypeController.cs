﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.DataLayer;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    [RoutePrefix("api/OrderType")]
    public class OrderTypeController : BaseController
    {
        private readonly IOrderTypeService _orderTypeService;
        public OrderTypeController()
        {
            _orderTypeService = new OrderTypeService();
        }

        [Route("GetByPage")]
        [HttpGet]
        public IHttpActionResult GetByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_orderTypeService.GetRoles(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetOrderType");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetSingle")]
        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            try
            {
                return Ok(_orderTypeService.GetSingle(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetOrdertype");
                return BadRequest(ex.Message);
            }
        }

        [Route("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(_orderTypeService.GetAll());
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetOrdertype");
                return BadRequest(ex.Message);
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult Create(OrderTypeDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_orderTypeService.Add(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveOrderType");
                return BadRequest(ex.Message);
            }
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update(OrderTypeDTO obj)
        {
            try
            {
                obj.UserId = base.LoggedInUserId;
                return Ok(_orderTypeService.Edit(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveOrderType");
                return BadRequest(ex.Message);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                OrderTypeDTO obj = new OrderTypeDTO();
                obj.Id = id;
                return Ok(_orderTypeService.Delete(obj));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteOrderType");
                return BadRequest(ex.Message);
            }
        }
    }
}