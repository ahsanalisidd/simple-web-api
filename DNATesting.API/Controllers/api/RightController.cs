﻿using EGR.Domain.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DNATesting.Api.Filters;
using DNATesting.Domain.Implementation;
using DNATesting.Domain.Model;

namespace DNATesting.Api.Controllers.Api
{
    [ValidateAccessToken]
    [RoutePrefix("right")]
    public class RightController : BaseController
    {
        private readonly RightService _rightService;
        public RightController()
        {
            _rightService = new RightService();
        }

        [HttpGet]
        public IHttpActionResult Get(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            try
            {
                return Ok(_rightService.GetRights(pageNo, noOfRecords, searchText));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRights");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_rightService.GetRight(id));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "GetRight");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Post(RightDTO right)
        {
            try
            {
                return Ok(_rightService.SaveRight(right));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "SaveRight");
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                RightDTO Right = new RightDTO();
                Right.Id = id;
                return Ok(_rightService.DeleteRight(Right));
            }
            catch (Exception ex)
            {
                base.Log(ex.Message, "DeleteRight");
                return BadRequest(ex.Message);
            }
        }
    }
}