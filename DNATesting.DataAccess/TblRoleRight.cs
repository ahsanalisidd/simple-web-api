namespace DNATesting.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TblRoleRight
    {
        public int Id { get; set; }

        public int? RoleId { get; set; }

        public int? RightId { get; set; }

        public DateTime? DeletedDate { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public virtual TblRight TblRight { get; set; }

        public virtual TblRole TblRole { get; set; }
    }
}
