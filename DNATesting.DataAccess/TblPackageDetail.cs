namespace DNATesting.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TblPackageDetail
    {
        public int Id { get; set; }

        public int? PackageId { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(150)]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual TblPackage TblPackage { get; set; }
    }
}
