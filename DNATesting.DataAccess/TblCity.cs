namespace DNATesting.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TblCity")]
    public partial class TblCity
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int? StateId { get; set; }

        public bool? IsActive { get; set; }

        public DateTime? DeletedDate { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public virtual TblState TblState { get; set; }

        public virtual TblUser TblUser { get; set; }

        public virtual TblUser TblUser1 { get; set; }
    }
}
