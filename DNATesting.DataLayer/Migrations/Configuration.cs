namespace DNATesting.DataLayer.Migrations
{
    using DNATesting.Helper;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DNATesting.DataLayer.DBContext.DNATestingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DNATesting.DataLayer.DBContext.DNATestingContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            Role objtblRole = new Role();
            objtblRole.CreatedOn = DateTime.Now;
            objtblRole.Name = "Admin";
            objtblRole.IsActive = true;
            context.Role.Add(objtblRole);


            User objtblUser = new User();
            objtblUser.CreatedOn = DateTime.Now;
            objtblUser.FirstName = "DNA Testing";
            objtblUser.LastName = "Admin";
            objtblUser.RoleId = 1;
            objtblUser.UserName = "admin";
            objtblUser.IsActive = true;
            objtblUser.Password = Crypto.SHA512Enc("admin");
            context.User.Add(objtblUser);

            base.Seed(context);
        }
    }
}
