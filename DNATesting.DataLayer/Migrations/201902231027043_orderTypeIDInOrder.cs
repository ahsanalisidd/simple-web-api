namespace DNATesting.DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderTypeIDInOrder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        StateId = c.Int(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.States", t => t.StateId)
                .Index(t => t.StateId)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        RoleId = c.Int(),
                        ProfilePicture = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        Role_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Roles", t => t.Role_Id)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.RoleId)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.Role_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        CurrencySymbol = c.String(),
                        PhoneCode = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        CountryId = c.Int(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.CountryId)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Coupons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        OrderDate = c.DateTime(),
                        Description = c.String(),
                        CustomerId = c.Int(),
                        CurrentStatus = c.String(),
                        OrderRecievingType = c.Int(nullable: false),
                        Country = c.String(),
                        State = c.String(),
                        City = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        TotalAmount = c.Decimal(precision: 18, scale: 2),
                        CouponId = c.Int(),
                        DiscountOnCoupon = c.Decimal(precision: 18, scale: 2),
                        NetAmount = c.Decimal(precision: 18, scale: 2),
                        PaymentMode = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.OrderTypes", t => t.OrderRecievingType, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.CustomerId)
                .Index(t => t.OrderRecievingType)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Contact = c.String(),
                        Landline = c.String(),
                        DOB = c.DateTime(),
                        Gender = c.String(),
                        Password = c.String(),
                        Picture = c.String(),
                        IsAccountActive = c.Boolean(),
                        IsTemporaryLock = c.Boolean(),
                        ActivateAccountHash = c.String(),
                        ResetPasswordHash = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        State = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(),
                        PackageId = c.Int(),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                        Rate = c.Decimal(precision: 18, scale: 2),
                        TotalAmount = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .ForeignKey("dbo.Packages", t => t.PackageId)
                .Index(t => t.OrderId)
                .Index(t => t.PackageId);
            
            CreateTable(
                "dbo.Packages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        PackageTypeId = c.Int(),
                        TotalAmount = c.Decimal(precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        NetAmount = c.Decimal(precision: 18, scale: 2),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.PackageTypes", t => t.PackageTypeId)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.PackageTypeId)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.PackageDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PackageId = c.Int(),
                        Code = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Packages", t => t.PackageId)
                .Index(t => t.PackageId);
            
            CreateTable(
                "dbo.PackageTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.OrderTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.RoleRights",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(),
                        RightId = c.Int(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .ForeignKey("dbo.Rights", t => t.RightId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.RightId)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
            CreateTable(
                "dbo.Rights",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OrderNo = c.Int(),
                        IsActive = c.Boolean(),
                        CreatedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PageTitle = c.String(),
                        PageName = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.ModifiedBy)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Contents", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Cities", "StateId", "dbo.States");
            DropForeignKey("dbo.Cities", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Cities", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Users", "User_Id", "dbo.Users");
            DropForeignKey("dbo.States", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Roles", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.RoleRights", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RoleRights", "RightId", "dbo.Rights");
            DropForeignKey("dbo.RoleRights", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.RoleRights", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Roles", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Roles", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.PackageTypes", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Packages", "User_Id", "dbo.Users");
            DropForeignKey("dbo.OrderTypes", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Orders", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Orders", "OrderRecievingType", "dbo.OrderTypes");
            DropForeignKey("dbo.OrderTypes", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.OrderTypes", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.OrderDetails", "PackageId", "dbo.Packages");
            DropForeignKey("dbo.Packages", "PackageTypeId", "dbo.PackageTypes");
            DropForeignKey("dbo.PackageTypes", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.PackageTypes", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.PackageDetails", "PackageId", "dbo.Packages");
            DropForeignKey("dbo.Packages", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Packages", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Customers", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Customers", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Orders", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Users", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Users", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Coupons", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Coupons", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Coupons", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Countries", "User_Id", "dbo.Users");
            DropForeignKey("dbo.States", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.States", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.States", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Countries", "ModifiedBy", "dbo.Users");
            DropForeignKey("dbo.Countries", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Cities", "User_Id", "dbo.Users");
            DropIndex("dbo.Contents", new[] { "ModifiedBy" });
            DropIndex("dbo.Contents", new[] { "CreatedBy" });
            DropIndex("dbo.RoleRights", new[] { "ModifiedBy" });
            DropIndex("dbo.RoleRights", new[] { "CreatedBy" });
            DropIndex("dbo.RoleRights", new[] { "RightId" });
            DropIndex("dbo.RoleRights", new[] { "RoleId" });
            DropIndex("dbo.Roles", new[] { "User_Id" });
            DropIndex("dbo.Roles", new[] { "ModifiedBy" });
            DropIndex("dbo.Roles", new[] { "CreatedBy" });
            DropIndex("dbo.OrderTypes", new[] { "User_Id" });
            DropIndex("dbo.OrderTypes", new[] { "ModifiedBy" });
            DropIndex("dbo.OrderTypes", new[] { "CreatedBy" });
            DropIndex("dbo.PackageTypes", new[] { "User_Id" });
            DropIndex("dbo.PackageTypes", new[] { "ModifiedBy" });
            DropIndex("dbo.PackageTypes", new[] { "CreatedBy" });
            DropIndex("dbo.PackageDetails", new[] { "PackageId" });
            DropIndex("dbo.Packages", new[] { "User_Id" });
            DropIndex("dbo.Packages", new[] { "ModifiedBy" });
            DropIndex("dbo.Packages", new[] { "CreatedBy" });
            DropIndex("dbo.Packages", new[] { "PackageTypeId" });
            DropIndex("dbo.OrderDetails", new[] { "PackageId" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.Customers", new[] { "ModifiedBy" });
            DropIndex("dbo.Customers", new[] { "CreatedBy" });
            DropIndex("dbo.Orders", new[] { "User_Id" });
            DropIndex("dbo.Orders", new[] { "ModifiedBy" });
            DropIndex("dbo.Orders", new[] { "CreatedBy" });
            DropIndex("dbo.Orders", new[] { "OrderRecievingType" });
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            DropIndex("dbo.Coupons", new[] { "User_Id" });
            DropIndex("dbo.Coupons", new[] { "ModifiedBy" });
            DropIndex("dbo.Coupons", new[] { "CreatedBy" });
            DropIndex("dbo.States", new[] { "User_Id" });
            DropIndex("dbo.States", new[] { "ModifiedBy" });
            DropIndex("dbo.States", new[] { "CreatedBy" });
            DropIndex("dbo.States", new[] { "CountryId" });
            DropIndex("dbo.Countries", new[] { "User_Id" });
            DropIndex("dbo.Countries", new[] { "ModifiedBy" });
            DropIndex("dbo.Countries", new[] { "CreatedBy" });
            DropIndex("dbo.Users", new[] { "User_Id" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Users", new[] { "ModifiedBy" });
            DropIndex("dbo.Users", new[] { "CreatedBy" });
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Cities", new[] { "User_Id" });
            DropIndex("dbo.Cities", new[] { "ModifiedBy" });
            DropIndex("dbo.Cities", new[] { "CreatedBy" });
            DropIndex("dbo.Cities", new[] { "StateId" });
            DropTable("dbo.Contents");
            DropTable("dbo.Rights");
            DropTable("dbo.RoleRights");
            DropTable("dbo.Roles");
            DropTable("dbo.OrderTypes");
            DropTable("dbo.PackageTypes");
            DropTable("dbo.PackageDetails");
            DropTable("dbo.Packages");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Customers");
            DropTable("dbo.Orders");
            DropTable("dbo.Coupons");
            DropTable("dbo.States");
            DropTable("dbo.Countries");
            DropTable("dbo.Users");
            DropTable("dbo.Cities");
        }
    }
}
