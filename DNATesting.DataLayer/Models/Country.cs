namespace DNATesting.DataLayer
{
    using DNATesting.DataLayer.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Country : TblBase
    {
        public Country()
        {
            this.States = new HashSet<State>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public string CurrencySymbol { get; set; }
        public string PhoneCode { get; set; }
        public virtual ICollection<State> States { get; set; }
    }
}
