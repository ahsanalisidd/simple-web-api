
using DNATesting.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DNATesting.DataLayer
{
    public partial class Content : TblBase
    {
        public string PageTitle { get; set; }
        public string PageName { get; set; }
        public string Description { get; set; }
    }
}
