﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNATesting.DataLayer
{
    using DNATesting.DataLayer.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Order : TblBase
    {
        public Order()
        {
            this.OrderDetails = new HashSet<OrderDetail>();
        }

        public string Code { get; set; }
        public DateTime? OrderDate { get; set; }
        public string Description { get; set; }
        [ForeignKey("Customer")]
        public Nullable<int> CustomerId { get; set; }
        public string CurrentStatus { get; set; }
        [ForeignKey("OrderType")]
        public int OrderRecievingType { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public decimal? TotalAmount { get; set; }
        public int? CouponId { get; set; }
        public decimal? DiscountOnCoupon { get; set; }
        public decimal? NetAmount { get; set; }
        public string PaymentMode { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual OrderType OrderType { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
