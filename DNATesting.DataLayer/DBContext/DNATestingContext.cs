﻿using DNATesting.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.DataLayer.DBContext
{
   public class DNATestingContext : DbContext
    {
        public DNATestingContext() : base("name=DNATestingConnectionString") // Connection String
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
        // Tables goin to create in Database
        public DbSet<Role> Role { get; set; }
        public DbSet<Right> Right { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<RoleRight> RoleRight { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Coupon> Coupon { get; set; }
        public DbSet<PackageType> PackageType { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<OrderType> OrderType { get; set; }
        public DbSet<Package> Package { get; set; }
        public DbSet<PackageDetail> PackageDetail { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<Content> Content { get; set; }
    }
}
