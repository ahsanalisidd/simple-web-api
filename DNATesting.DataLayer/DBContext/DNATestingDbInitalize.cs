﻿using DNATesting.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.DataLayer.DBContext
{
    class DNATestingDbInitalize : DropCreateDatabaseIfModelChanges<DNATestingContext>
    {
        protected override void Seed(DNATestingContext context)
        {
            context.SaveChanges();

            base.Seed(context);

        }
    }

}
