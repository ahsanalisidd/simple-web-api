﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class PackageRepository : IPackageRepository
    {
        protected DNATestingContext _entities;
        protected readonly IDbSet<Package> _dbset;

        public PackageRepository()
        {
            _entities = new DNATestingContext();
            _dbset = _entities.Set<Package>();
        }

        public virtual IEnumerable<Package> GetAll()
        {
            var result = _dbset.Include(x=>x.PackageDetails).Where(x => x.DeletedDate == null).AsEnumerable<Package>();
            return result;
        }

        public IEnumerable<Package> FindBy(Expression<Func<Package, bool>> predicate)
        {

            IEnumerable<Package> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsEnumerable();
            return query;
        }

        public virtual Package Add(Package entity)
        {
            return _dbset.Add(entity);
        }

        public virtual Package Delete(Package entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(Package entity)
        {
            if (entity.Id != 0)
            {
                var oldDetails = _entities.PackageDetail.Where(detail => detail.PackageId == entity.Id);
                foreach (var oldDetail in oldDetails)
                {
                    _entities.PackageDetail.Remove(oldDetail);
                }
                foreach (var detail in entity.PackageDetails)
                {
                    detail.PackageId = entity.Id;
                    _entities.Entry(detail).State = EntityState.Added;
                }
            }
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public IQueryable<Package> Find(Expression<Func<Package, bool>> predicate)
        {
            IQueryable<Package> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsQueryable();
            return query;
        }

        public void SoftDelete(Package entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
