﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class CouponRepository : ICouponRepository
    {
        protected DNATestingContext _entities;
        protected readonly IDbSet<Coupon> _dbset;

        public CouponRepository()
        {
            _entities = new DNATestingContext();
            _dbset = _entities.Set<Coupon>();
        }

        public virtual IEnumerable<Coupon> GetAll()
        {
            var result = _dbset.Where(x=>x.DeletedDate == null).AsEnumerable<Coupon>();
            return result;
        }

        public IEnumerable<Coupon> FindBy(Expression<Func<Coupon, bool>> predicate)
        {

            IEnumerable<Coupon> query = _dbset.Where(predicate).Where(x=>x.DeletedDate == null).AsEnumerable();
            return query;
        }

        public virtual Coupon Add(Coupon entity)
        {
            return _dbset.Add(entity);
        }

        public virtual Coupon Delete(Coupon entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(Coupon entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public IQueryable<Coupon> Find(Expression<Func<Coupon, bool>> predicate)
        {
            IQueryable<Coupon> query = _dbset.Where(predicate).Where(x=>x.DeletedDate == null).AsQueryable();
            return query;
        }

        public void SoftDelete(Coupon entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
