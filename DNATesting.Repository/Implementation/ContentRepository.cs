﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class ContentRepository : IContentRepository
    {
        protected DNATestingContext _entities;
        protected readonly IDbSet<Content> _dbset;

        public ContentRepository()
        {
            _entities = new DNATestingContext();
            _dbset = _entities.Set<Content>();
        }

        public virtual IEnumerable<Content> GetAll()
        {
            var result = _dbset.Where(x => x.DeletedDate == null).AsEnumerable<Content>();
            return result;
        }

        public IEnumerable<Content> FindBy(Expression<Func<Content, bool>> predicate)
        {

            IEnumerable<Content> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsEnumerable();
            return query;
        }

        public virtual Content Add(Content entity)
        {
            return _dbset.Add(entity);
        }

        public virtual Content Delete(Content entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(Content entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public IQueryable<Content> Find(Expression<Func<Content, bool>> predicate)
        {
            IQueryable<Content> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsQueryable();
            return query;
        }

        public void SoftDelete(Content entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
