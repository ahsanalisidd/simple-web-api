﻿using DNATesting.DataLayer.DBContext;
using DNATesting.DataLayer.Models;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class AuthRepository 
    {
        private readonly DNATestingContext context;
        public AuthRepository()
        {
            context = new DNATestingContext();
        }

        public IList<Customer> GetCustomers()
        {
            var Customerswithauthors = context.Customers.ToList();

            return Customerswithauthors;
        }
    }
}
