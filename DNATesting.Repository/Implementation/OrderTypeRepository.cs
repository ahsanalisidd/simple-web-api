﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class OrderTypeRepository : IOrderTypeRepository
    {
        protected DNATestingContext _entities;
        protected readonly IDbSet<OrderType> _dbset;

        public OrderTypeRepository()
        {
            _entities = new DNATestingContext();
            _dbset = _entities.Set<OrderType>();
        }

        public virtual IEnumerable<OrderType> GetAll()
        {
            var result = _dbset.Where(x => x.DeletedDate == null).AsEnumerable<OrderType>();
            return result;
        }

        public IEnumerable<OrderType> FindBy(Expression<Func<OrderType, bool>> predicate)
        {

            IEnumerable<OrderType> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsEnumerable();
            return query;
        }

        public virtual OrderType Add(OrderType entity)
        {
            return _dbset.Add(entity);
        }

        public virtual OrderType Delete(OrderType entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(OrderType entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public IQueryable<OrderType> Find(Expression<Func<OrderType, bool>> predicate)
        {
            IQueryable<OrderType> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsQueryable();
            return query;
        }

        public void SoftDelete(OrderType entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
