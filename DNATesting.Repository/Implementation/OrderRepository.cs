﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class OrderRepository : IOrderRepository
    {
        protected DNATestingContext _entities;
        protected readonly IDbSet<Order> _dbset;

        public OrderRepository()
        {
            _entities = new DNATestingContext();
            _dbset = _entities.Set<Order>();
        }

        public virtual IEnumerable<Order> GetAll()
        {
            var result = _dbset.Where(x => x.DeletedDate == null).Include(x => x.OrderDetails).AsEnumerable<Order>();
            return result;
        }

        public IEnumerable<Order> FindBy(Expression<Func<Order, bool>> predicate)
        {

            IEnumerable<Order> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).Include(x => x.OrderDetails).AsEnumerable();
            return query;
        }

        public virtual Order Add(Order entity)
        {
            return _dbset.Add(entity);
        }

        public virtual Order Delete(Order entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(Order entity)
        {
            {
                if (entity.Id != 0)
                {
                    var oldDetails = _entities.OrderDetail.Where(detail => detail.OrderId == entity.Id);
                    foreach (var oldDetail in oldDetails)
                    {
                        _entities.OrderDetail.Remove(oldDetail);
                    }
                    foreach (var detail in entity.OrderDetails)
                    {
                        detail.OrderId = entity.Id;
                        _entities.Entry(detail).State = EntityState.Added;
                    }
                }
                _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            }
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public IQueryable<Order> Find(Expression<Func<Order, bool>> predicate)
        {
            IQueryable<Order> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).Include(x => x.OrderDetails).AsQueryable();
            return query;
        }

        public void SoftDelete(Order entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
