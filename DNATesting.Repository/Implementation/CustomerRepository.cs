﻿using DNATesting.DataLayer.DBContext;
using DNATesting.DataLayer.Models;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class CustomerRepository : ICustomerRepository
    {
        protected DNATestingContext _entities;
        protected readonly IDbSet<Customer> _dbset;

        public CustomerRepository()
        {
            _entities = new DNATestingContext();
            _dbset = _entities.Set<Customer>();
        }

        public virtual IEnumerable<Customer> GetAll()
        {
            var result = _dbset.Where(x => x.DeletedDate == null).AsEnumerable<Customer>();
            return result;
        }

        public IEnumerable<Customer> FindBy(Expression<Func<Customer, bool>> predicate)
        {

            IEnumerable<Customer> query = _dbset.AsNoTracking().Where(predicate).Where(x => x.DeletedDate == null).AsEnumerable();
            return query;
        }

        public virtual Customer Add(Customer entity)
        {
            return _dbset.Add(entity);
        }

        public virtual Customer Delete(Customer entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(Customer entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }
        public void Detach(Customer entity)
        {
            _entities.Entry(entity).State = EntityState.Detached;
        }

        public IQueryable<Customer> Find(Expression<Func<Customer, bool>> predicate)
        {
            IQueryable<Customer> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsQueryable();
            return query;
        }

        public void SoftDelete(Customer entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public int MaxId()
        {
            int max = _dbset.Max(p => p.Id);
            return max;
        }
    }
}
