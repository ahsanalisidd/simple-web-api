﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Implementation
{
    public class PackageTypeRepository : IPackageTypeRepository
    {
        protected DNATestingContext _entities;
        protected readonly IDbSet<PackageType> _dbset;

        public PackageTypeRepository()
        {
            _entities = new DNATestingContext();
            _dbset = _entities.Set<PackageType>();
        }

        public virtual IEnumerable<PackageType> GetAll()
        {
            var result = _dbset.Where(x => x.DeletedDate == null).AsEnumerable<PackageType>();
            return result;
        }

        public IEnumerable<PackageType> FindBy(Expression<Func<PackageType, bool>> predicate)
        {

            IEnumerable<PackageType> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsEnumerable();
            return query;
        }

        public virtual PackageType Add(PackageType entity)
        {
            return _dbset.Add(entity);
        }

        public virtual PackageType Delete(PackageType entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(PackageType entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public void SoftDelete(PackageType entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public IQueryable<PackageType> Find(Expression<Func<PackageType, bool>> predicate)
        {
            IQueryable<PackageType> query = _dbset.Where(predicate).Where(x => x.DeletedDate == null).AsQueryable();
            return query;
        }

    }
}
