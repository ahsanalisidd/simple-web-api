﻿using DNATesting.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Infrastructure
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        void Detach(Customer entity);
        int MaxId();
    }
}
