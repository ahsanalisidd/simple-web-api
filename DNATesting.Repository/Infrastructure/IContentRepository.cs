﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Repository.Infrastructure
{
    public interface IContentRepository : IGenericRepository<Content>
    {
    }
}
