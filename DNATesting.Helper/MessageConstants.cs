﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DNATesting.Helper
{
    public class MessageConstants
    {
        public static string ServerError = "Oh something bad happend on server.";
        public static string AccessTokenFailed = "Access token validation failed.";
        public static string AuthrizationFailed = "Unauthorized: Access is denied due to invalid credentials.";
        public static string InvalidCredentials = "Invalid username or password";
        public static string BadRequest = "Your browser sent a request that this server could not understand.";
        public static string Success = "Success";
        public static string NotFound = "{type} not found.";
        public static string AlreadyExists = "{type} already exists.";
        public static string AccountDeactivated = "You account has been deactivated please contact administrator.";
        public static string LoginSuccess = "You are successfully logged in.";
        public static string LogoutSuccess = "You are successfully logout.";
        public static string DirectoryNotExists = "Directory not exists";
        public static string ImageUploadError = "Oh there was some error uploading image";
        public static string EmailAddressAlreadyExists = "Email address already exists";
        public static string UserNameAlreadyExists = "Username already exists";
        public static string AccountNotActivate = "You account is not activated";
        public static string AccountTemporaryLock = "You account has been temporary locked";
    }
}