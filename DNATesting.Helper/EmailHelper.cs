﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Configuration;
using System.Net;
using System.Text;
using System.IO;

namespace DNATesting.Helper
{
    public class EmailHelper
    {
        public static void SendEmails(List<string> emails, string subject, string body)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SMTPEMAIL"]);
                foreach (string item in emails)
                {
                    mail.To.Add(item);
                }
                mail.IsBodyHtml = true;
                mail.Subject = subject;
                mail.Body = body;

                SmtpClient client = new SmtpClient();
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPORT"]);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = true;
                client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPEMAIL"], ConfigurationManager.AppSettings["SMTPPASSWORD"]);
                client.Host = ConfigurationManager.AppSettings["SMTPHOST"];
                client.Send(mail);
            }
            catch (Exception ex)
            {

            }
        }

        public static bool SendEmail(string toEmail, string subject, string templateName, Dictionary<string, string> data)
        {

            try
            {
                string template = File.ReadAllText(HttpContext.Current.Server.MapPath("/templates/" + templateName + ".html"));
                foreach (var item in data)
                {
                    template = template.Replace(item.Key, item.Value);
                }
                template = template.Replace("{currentYear}", DateTime.Now.Year.ToString());
                template = template.Replace("{appName}", ConfigurationManager.AppSettings["APPNAME"]);
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString();

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);

                MailMessage mailMessage = new MailMessage(senderEmail, toEmail, subject, template);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                client.Send(mailMessage);

                return true;

            }
            catch (Exception ex)
            {
                return false;

            }

        }
    }
}