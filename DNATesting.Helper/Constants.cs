﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DNATesting.Helper
{
    public class Constants
    {
        public static string EncryptionKey = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1theclassicgym";
        public static int MinimumInvalidAttempts = 3;
        public static int DefaultSkip = 0;
        public static int DefaultTake = 10000000;
    }
}