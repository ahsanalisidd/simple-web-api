﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Helper
{
    public class EnumHelper
    {
        public enum SaleStatus
        {
            UNPAID = 1,
            PAID = 2,
            PARTAIL_PAID = 3,
            RETURN = 4
        }

        public enum VoucherTypes
        {
            BP = 1,
            BR = 2,
            CP = 3,
            CR = 4,
            JV = 5
        }
        public enum VoucherStatus
        {
            CANCELLED = 1,
            POSTED = 2,
            UNPOSTED = 3
        }
        public enum VoucherTypeCode
        {
            JournalVoucharCode = 1,
            BankPaymentVoucherCode = 2,
            CashPaymentVoucherCode = 3,
            CashReceiptVoucherCode = 4,
            BankReceiptVoucherCode = 5
        }

        public enum AutoGenerateCode
        {
            SalesReceiptNo = 1,
            ChartOfAccountCode = 2,
            CustomerCode = 3,
            SupplierCode = 4,
            ProductCode = 5,
            WhiteReportCode = 6,
            ProductionCode = 6,
            RefineryCode = 7,
            PurchaseCode = 2028
        }

        #region Get Configuration Params
        public static int GetTokenExpiration
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["TokenExpiration"]);
            }
        }
        #endregion
    }
}
