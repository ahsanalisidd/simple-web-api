﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace DNATesting.Helper
{
    public class GeneralHelper
    {
        public static string GeneratePassword(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public static string GenerateId()
        {
            Random random = new Random();
            string d = DateTime.Now.Ticks.ToString();
            string st = new string(Enumerable.Repeat(d, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            long i = Convert.ToInt64(st) + Convert.ToInt64(9876543210) + Convert.ToInt64(st) + Convert.ToInt64(0123456789);
            string chars = i.ToString();
            return new string(Enumerable.Repeat(chars, 20)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateAccessToken(string UserId, string StoreId)
        {
            string key = DateTime.Now.Millisecond.ToString() + "u=" + UserId + "s=" + StoreId + "d=" + DateTime.Now.ToString();
            return Crypto.SHA512Enc(key);
        }

        public static string formatImagePath(string imageName, string storeId)
        {
            string[] url = HttpContext.Current.Request.Url.AbsoluteUri.Split('/');
            return ConfigurationManager.AppSettings["APIBaseURL"] + "upload/" + storeId + "/" + imageName;
        }
        public static DateTime EndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
        }

        public static DateTime StartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}