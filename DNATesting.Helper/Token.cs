﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Helper
{
    public class Token
    {
        public static string GenerateToken(int? userId)
        {
            Microsoft.IdentityModel.Logging.IdentityModelEventSource.ShowPII = true;
            var key = Encoding.ASCII.GetBytes(Constants.EncryptionKey);

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, userId.ToString())
            }),
                Expires = DateTime.UtcNow.AddMinutes(EnumHelper.GetTokenExpiration),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Sid, userId.ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64));
            claims.Add(new Claim(JwtRegisteredClaimNames.Exp, DateTimeOffset.UtcNow.AddMinutes(EnumHelper.GetTokenExpiration).ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64));
            var jwt = new JwtSecurityToken(
            claims: claims);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            ValidateToken(encodedJwt);

            return encodedJwt;
        }

        public static int? ValidateToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            JwtSecurityToken decodedToken = handler.ReadJwtToken(token);
            IList<Claim> claims = decodedToken.Claims.ToList();
            int userId = Convert.ToInt32(claims.Where(c => c.Type == JwtRegisteredClaimNames.Sid).Select(c => c.Value).SingleOrDefault());
            long exp = Convert.ToInt64(claims.Where(c => c.Type == JwtRegisteredClaimNames.Exp).Select(c => c.Value).SingleOrDefault());
            long currTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            if (currTime <= exp)
            {
                return userId;
            }
            return null;
        }
    }
}
