﻿using DNATesting.DataLayer;
using DNATesting.DataLayer.Models;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Content;
using DNATesting.Domain.Model.Coupon;
using DNATesting.Domain.Model.Customer;
using DNATesting.Domain.Model.Order;
using DNATesting.Domain.Model.package;
using DNATesting.Domain.Model.Public;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Mapper
{
    public static class MappingConfig
    {
        public static void RegisterMapping()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<PackageType, PackageTypeDTO>().ReverseMap();
                cfg.CreateMap<PackageType, GetPackageTypeDTO>().ReverseMap();
                cfg.CreateMap<Customer, CustomerDTO>().ReverseMap();
                cfg.CreateMap<Customer, SignUpDTO>().ReverseMap();
                cfg.CreateMap<Customer, GetCustomerDTO>().ReverseMap();
                cfg.CreateMap<Customer, PubCustomerDTO>().ReverseMap();
                cfg.CreateMap<GetCouponDTO, Coupon>().ReverseMap();
                cfg.CreateMap<Coupon, CouponDTO>().ReverseMap();
                cfg.CreateMap<OrderType, OrderTypeDTO>().ReverseMap();
                cfg.CreateMap<GetOrderTypeDTO, OrderType>().ReverseMap();
                cfg.CreateMap<PackageDetail, PackageDetailDTO>().ReverseMap();
                cfg.CreateMap<OrderDetail, OrderDetailDTO>().ReverseMap();

                cfg.CreateMap<Content, ContentDTO>().ReverseMap();
                cfg.CreateMap<GetContentDTO, Content>().ReverseMap();

                cfg.CreateMap<Package, PackageDTO>()
                .ForMember(dto => dto.PackageDetailDTOs, opt => opt.MapFrom(src => src.PackageDetails));
                cfg.CreateMap<PackageDTO, Package>()
                .ForMember(dto => dto.PackageDetails, opt => opt.MapFrom(src => src.PackageDetailDTOs));

                cfg.CreateMap<Package, GetPackageDTO>()
                .ForMember(dto => dto.PackageDetailDTOs, opt => opt.MapFrom(src => src.PackageDetails));
                cfg.CreateMap<GetPackageDTO, Package>()
                .ForMember(dto => dto.PackageDetails, opt => opt.MapFrom(src => src.PackageDetailDTOs));
                cfg.CreateMap<Package, PubPackageDTO>()
                .ForMember(dto => dto.PackageDetailDTOs, opt => opt.MapFrom(src => src.PackageDetails));
                cfg.CreateMap<PubPackageDTO, Package>()
                .ForMember(dto => dto.PackageDetails, opt => opt.MapFrom(src => src.PackageDetailDTOs));


                cfg.CreateMap<Order, OrderDTO>()
                .ForMember(dto => dto.OrderDetailsDTO, opt => opt.MapFrom(src => src.OrderDetails));
                cfg.CreateMap<OrderDTO, Order>()
                .ForMember(dto => dto.OrderDetails, opt => opt.MapFrom(src => src.OrderDetailsDTO));

                cfg.CreateMap<Order, GetOrderDTO>()
                .ForMember(dto => dto.OrderDetailsDTO, opt => opt.MapFrom(src => src.OrderDetails));
                cfg.CreateMap<GetOrderDTO, Order>()
                .ForMember(dto => dto.OrderDetails, opt => opt.MapFrom(src => src.OrderDetailsDTO));
            });
        }
    }
}
