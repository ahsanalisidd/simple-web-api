﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.DataLayer.Models;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Customer;
using DNATesting.Domain.Model.Order;
using DNATesting.Domain.Model.Public;
using DNATesting.Helper;
using DNATesting.Repository.Implementation;
using DNATesting.Repository.Infrastructure;

namespace DNATesting.Domain.Implementation
{
    public class PublicService : IPublicService
    {

        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IPackageRepository _packageRepository;
        public PublicService()
        {
            _customerRepository = new CustomerRepository();
            _packageRepository = new PackageRepository();
            _orderRepository = new OrderRepository();
        }

        public SignUpDTO SignUp(SignUpDTO obj)
        {
            var exists = _customerRepository.Find(x => x.Email == obj.Email).FirstOrDefault();
            if (exists == null)
            {
                string accountHash = Token.GenerateToken(Convert.ToInt32(Crypto.CreateNumberPassword(6)));
                var res = AutoMapper.Mapper.Map(obj, new Customer());
                res.Code = new CustomerService().GetCode();
                res.CreatedOn = DateTime.UtcNow;
                res.IsTemporaryLock = false;
                res.IsActive = true;
                res.IsAccountActive = false;
                res.ActivateAccountHash = accountHash;
                res.Password = Crypto.SHA512Enc(obj.Password);
                var result = _customerRepository.Add(res);
                _customerRepository.Save();

                //Email Send Code
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("{username}", obj.FirstName);
                data.Add("{url}", ConfigurationManager.AppSettings["WEBSITEURL"] + "/verify/" + res.ActivateAccountHash);
                var IsEmailSend = EmailHelper.SendEmail(obj.Email, "Account Verification.", "signup", data);
                obj.Password = null;
                return obj;
            }
            else
            {
                throw new Exception("Email Already exists");
            }
        }

        public PubCustomerDTO EditCustomer(PubCustomerDTO entity)
        {
            var exist = _customerRepository.FindBy(x => x.Email == entity.Email).FirstOrDefault();
            
            if (exist != null)
            {
                _customerRepository.Detach(exist);
                var found = _customerRepository.FindBy(x => x.Email == entity.Email).FirstOrDefault();
                found.FirstName = entity.FirstName;
                found.LastName = entity.LastName;
                found.MiddleName = entity.MiddleName;
                found.Contact = entity.Contact;
                found.Landline = entity.Landline;
                found.DOB = entity.DOB;
                found.Gender = entity.Gender;
                found.Country = entity.Country;
                found.State = entity.State;
                found.City = entity.City;
                found.AddressLine1 = entity.AddressLine1;
                found.AddressLine2 = entity.AddressLine2;
                found.ModifiedOn = DateTime.UtcNow;
                _customerRepository.Detach(found);
                _customerRepository.Edit(found);
                _customerRepository.Save();
                return AutoMapper.Mapper.Map(found, new PubCustomerDTO());
            }
            else
            {
                throw new Exception("User does not exists");
            }
        }

        public bool VerificationByEmail(string email)
        {
            var accountHash = Token.GenerateToken(Convert.ToInt32(Crypto.CreateNumberPassword(6)));
            var exists = _customerRepository.Find(x => x.Email == email && x.IsAccountActive == false && x.DeletedDate == null).FirstOrDefault();
            if (exists != null)
            {
                //Email Send Code
                exists.ActivateAccountHash = accountHash;
                _customerRepository.Edit(exists);
                _customerRepository.Save();

                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("{username}", exists.FirstName);
                data.Add("{url}", ConfigurationManager.AppSettings["WEBSITEURL"] + "/verify/" + exists.ActivateAccountHash);
                var IsEmailSend = EmailHelper.SendEmail(exists.Email, "Account Verification.", "signup", data);
                return true;
            }
            else
            {
                throw new Exception("Email Not Exists");
            }
        }

        public Object SignIn(SignInDTO obj)
        {
            var Encrypt = Crypto.SHA512Enc(obj.Password);
            var res = _customerRepository.Find(x => x.Email == obj.Email && x.Password == Encrypt
            && x.IsTemporaryLock == false && x.IsActive == true && x.DeletedDate == null).FirstOrDefault();

            if (res != null)
            {
                if (res.IsAccountActive == false)
                {
                    return new { Code = 1002, Message = "Your account has been not verified please verify your account." };
                }
                else
                {
                    
                    res.Password = null;
                    var userResp = AutoMapper.Mapper.Map(res, new SignUpDTO());
                    userResp.AccessToken = Token.GenerateToken(res.Id);
                    return new { Code = 200, Data = userResp }; ;
                }
            }
            else
            {
                return null;
            }
        }

        public bool ForgotPassword(string email)
        {
            var accountHash = Token.GenerateToken(Convert.ToInt32(Crypto.CreateNumberPassword(6)));
            var res = _customerRepository.Find(x => x.Email == email && x.IsAccountActive == true && x.IsTemporaryLock == false && x.IsActive == true && x.DeletedDate == null).FirstOrDefault();
            if (res != null)
            {
                res.ResetPasswordHash = accountHash;
                _customerRepository.Edit(res);
                _customerRepository.Save();

                //Email Send Code
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("{username}", res.FirstName);
                data.Add("{url}", ConfigurationManager.AppSettings["WEBSITEURL"] + "/reset/" + res.ResetPasswordHash);
                var IsEmailSend = EmailHelper.SendEmail(res.Email, "Reset Password.", "forgotPassword", data);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool UpdatePassword(ForgotPasswordDTO obj)
        {
            var res = _customerRepository.Find(x => x.ResetPasswordHash == obj.VerificationCode).FirstOrDefault();

            if (res != null)
            {
                int? dec = Token.ValidateToken(obj.VerificationCode);
                if (dec != null)
                {
                    res.Password = Crypto.SHA512Enc(obj.NewPassword);
                    res.ResetPasswordHash = null;
                    _customerRepository.Edit(res);
                    _customerRepository.Save();

                    //Email Send Code
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data.Add("{username}", res.FirstName);
                    var IsEmailSend = EmailHelper.SendEmail(res.Email, "Password Change.", "resetSuccess", data);

                    return true;
                }
                throw new Exception("Reset password link invalid or expired.");
            }
            else
            {
                return false;
            }
        }

        public SignUpDTO VerifyAccount(AccountVerifyDTO obj)
        {
            var res = _customerRepository.Find(x => x.ActivateAccountHash == obj.VerificationCode).FirstOrDefault();

            if (res != null)
            {
                int? dec = Token.ValidateToken(obj.VerificationCode);
                if (dec != null)
                {
                    res.IsAccountActive = true;
                    res.ActivateAccountHash = null;
                    res.IsActive = true;
                    _customerRepository.Edit(res);
                    _customerRepository.Save();
                    res.Password = null;
                    return AutoMapper.Mapper.Map(res, new SignUpDTO());
                }
                throw new Exception("Verify account link invalid or expired.");
            }
            else
            {
                return null;
            }
        }

        public List<GetOrderDTO> GetAllOrder()
        {
            var res = _orderRepository.GetAll();
            if (res != null)
            {
                return AutoMapper.Mapper.Map(res, new List<GetOrderDTO>());
            }
            else
            {
                return null;
            }
        }

        public GetOrderDTO GetSingleOrder(int id)
        {
            var res = _orderRepository.Find(x => x.Id == id).FirstOrDefault();
            if (res != null)
            {
                return AutoMapper.Mapper.Map(res, new GetOrderDTO());
            }
            else
            {
                return null;
            }
        }

        public GetOrderDTO CancelOrder(int id)
        {
            var res = _orderRepository.Find(x => x.Id == id).FirstOrDefault();
            res.CurrentStatus = "CANCELLED";
            _orderRepository.Edit(res);
            _orderRepository.Save();
            if (res != null)
            {
                return AutoMapper.Mapper.Map(res, new GetOrderDTO());
            }
            else
            {
                return null;
            }
        }

        public GetOrderDTO CreateOrder(OrderDTO entity)
        {
            var res = AutoMapper.Mapper.Map(entity, new Order());
            //res.CreatedBy = entity.UserId;
            res.CreatedOn = DateTime.Now;
            var result = _orderRepository.Add(res);
            _orderRepository.Save();
            return AutoMapper.Mapper.Map(result, new GetOrderDTO());
        }

        public List<PubPackageDTO> GetPubPackage()
        {
            return AutoMapper.Mapper.Map(_packageRepository.GetAll(), new List<PubPackageDTO>());
        }

        public object GetPubPackageByPage(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _packageRepository.Find(col => col.DeletedDate == null);

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.Name.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderByDescending(col => col.CreatedOn);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<Package> response = asQueryable.ToList();
            var mapData = AutoMapper.Mapper.Map(response, new List<PubPackageDTO>());
            return new { TotalCount = totalRecordsCount, Data = mapData };
        }
    }
}
