﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataAccess;
using DNATesting.DataLayer.DBContext;
using DNATesting.Domain.Model;

namespace DNATesting.Domain.Implementation
{
    public class DashboardService
    {
        private readonly DNATestingContext _context;
        public DashboardService()
        {
            _context = new DNATestingContext();
        }
        public Object GetCounts()
        {
            DashboardCountDTO dto = new DashboardCountDTO();
            dto.UserCount = _context.User.Where(col => col.DeletedDate == null).Count();
            dto.RoleCount = _context.Role.Where(col => col.DeletedDate == null).Count();
            return new { Data = dto };
        }
        public Object GetCounts(DateTime? From, DateTime? To)
        {
            var orders = _context.Order.Where(col => col.DeletedDate == null).AsQueryable();
            if (From != null && To != null)
                orders = orders.Where(x => x.CreatedOn >= From && x.CreatedOn <= To);

            DashboardCountDTO dto = new DashboardCountDTO();
            dto.MaleCustomers = _context.Customers.Where(x => x.DeletedDate == null && x.Gender == "Male").Count();
            dto.FemaleCustomers = _context.Customers.Where(x => x.DeletedDate == null && x.Gender == "FeMale").Count();
            dto.NotSpecifiedCustomers = _context.Customers.Where(x => x.DeletedDate == null && x.Gender == "Not Specified").Count();
            dto.UserCount = _context.User.Where(col => col.DeletedDate == null).Count();
            dto.Packages = _context.Package.Where(x => x.DeletedDate == null).Count();
            dto.RoleCount = _context.Role.Where(col => col.DeletedDate == null).Count();
            dto.Approve = orders.Where(col => col.CurrentStatus == "DISPATCHED").Count();
            dto.Cancelled = orders.Where(col => col.CurrentStatus == "CANCELLED").Count();
            dto.Draft = orders.Where(col => col.CurrentStatus == "DRAFT").Count();
            dto.InProgress = orders.Where(col => col.CurrentStatus == "REPORTS IN PROGRESS").Count();
            return new { Data = dto };
        }


    }
}
