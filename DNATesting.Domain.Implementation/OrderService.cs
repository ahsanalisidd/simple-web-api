﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Order;
using DNATesting.Domain.Model.package;
using DNATesting.Repository.Implementation;
using DNATesting.Repository.Infrastructure;

namespace DNATesting.Domain.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _context;
        public OrderService()
        {
            _context = new OrderRepository();
        }

        public OrderDTO Add(OrderDTO entity)
        {
            var res = AutoMapper.Mapper.Map(entity, new Order());
            res.CreatedBy = entity.UserId;
            res.CreatedOn = DateTime.Now;
            var result = _context.Add(res);
            _context.Save();
            return AutoMapper.Mapper.Map( result,new OrderDTO());
        }

        public OrderDTO Delete(OrderDTO entity)
        { 
            var res = _context.Find(x => x.Id == entity.Id).FirstOrDefault();
            if (res != null)
            {
                res.DeletedDate = DateTime.UtcNow;
                _context.SoftDelete(res);
                _context.Save();
                return entity;
            }
            else
            {
                return entity;
            }
        }

        public OrderDTO Edit(OrderDTO entity)
        {
            var res = _context.Find(x => x.Id == entity.Id).Include(x => x.OrderDetails).FirstOrDefault();
            var order = AutoMapper.Mapper.Map(entity, res);
            order.ModifiedBy = entity.UserId;
            order.ModifiedOn = DateTime.UtcNow;
            _context.Edit(order);
            _context.Save();
            return AutoMapper.Mapper.Map(_context.Find(x=>x.Id == entity.Id).FirstOrDefault(), new OrderDTO());
        }

        public List<GetOrderDTO> GetAll()
        {
            List<GetOrderDTO> obj = new List<GetOrderDTO>();
            var list = _context.GetAll();
            return AutoMapper.Mapper.Map(list, obj);
        }

        public GetOrderDTO GetSingle(int id)
        {
            GetOrderDTO obj = new GetOrderDTO();
            var list = _context.Find(x=>x.Id == id).Include(x=>x.OrderDetails).FirstOrDefault();
            return AutoMapper.Mapper.Map(list, obj);
        }

        public object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _context.Find(col => col.DeletedDate == null);

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.Code.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderByDescending(col => col.CreatedOn);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<Order> response = asQueryable.ToList();
            var mapData = AutoMapper.Mapper.Map(response, new List<GetOrderDTO>());
            return new { TotalCount = totalRecordsCount, Data = mapData };
        }


        public Object GetOrderReport(OrderReportDTO obj)
        {
            var result = _context.Find(x => x.CreatedOn >= obj.FromDate && x.CreatedOn <= obj.ToDate);
            
            if (!string.IsNullOrEmpty(obj.Country))
                result = result.Where(x => x.Country == obj.Country);
            if (!string.IsNullOrEmpty(obj.State))
                result = result.Where(x => x.State == obj.State);
            if (!string.IsNullOrEmpty(obj.City))
                result = result.Where(x => x.City == obj.City);

            IList<Order> response = result.ToList();
            var mapData = AutoMapper.Mapper.Map(response, new List<GetOrderDTO>());

            return new { TotalCount = mapData.Count, Data = mapData };
        }

    }
}
