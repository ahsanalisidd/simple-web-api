﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Domain.Model;
using DNATesting.Helper;

namespace EGR.Domain.Implementation
{
    public class UserService
    {
        //private readonly TheClassicGymEntities _context;
        private readonly DNATestingContext _context;
        public UserService()
        {
            _context = new DNATestingContext();
        }

        public Object GetUsers(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _context.User.Where(col => col.DeletedDate == null).AsQueryable();

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.UserName.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderByDescending(col => col.ModifiedOn);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<UserDTO> response = asQueryable.Select(col => new UserDTO
            {
                Id = col.Id,
                FirstName = col.FirstName,
                LastName = col.LastName,
                UserName = col.UserName,
                ProfilePicture = col.ProfilePicture,
                RoleId = col.RoleId,
                RoleName = col.Role.Name,
                IsActive = col.IsActive,
                CreatedOn = col.CreatedOn,
                ModifiedOn = col.ModifiedOn,
                CreatedBy = col.CreatedBy,
                ModifiedBy = col.ModifiedBy
            }).ToList();


            return new { TotalCount = totalRecordsCount, Data = response };
        }
        public UserDTO GetUser(int? userId = null)
        {
            var asQueryable = _context.User.Where(col => col.DeletedDate == null && col.Id == userId.Value).AsQueryable();

            UserDTO response = asQueryable.Select(col => new UserDTO
            {
                Id = col.Id,
                FirstName = col.FirstName,
                LastName = col.LastName,
                UserName = col.UserName,
                ProfilePicture = col.ProfilePicture,
                RoleId = col.RoleId,
                RoleName = col.Role.Name,
                IsActive = col.IsActive,
                CreatedOn = col.CreatedOn,
                ModifiedOn = col.ModifiedOn,
                CreatedBy = col.CreatedBy,
                ModifiedBy = col.ModifiedBy
            }).FirstOrDefault();


            return response;
        }

        public UserDTO SaveUser(UserDTO user, int? LoggedInUser)
        {
            User objUser;
            if (user.Id > 0)
            {
                objUser = _context.User.Where(col => col.Id == user.Id).FirstOrDefault();
                if (objUser == null)
                    return null;

                objUser.ModifiedOn = DateTime.Now;
                objUser.ModifiedBy = LoggedInUser;
            }
            else
            {
                objUser = new User();
                objUser.CreatedOn = DateTime.Now;
                objUser.CreatedBy = LoggedInUser;
            }

            objUser.FirstName = user.FirstName;
            objUser.LastName = user.LastName;
            objUser.ProfilePicture = user.ProfilePicture;
            objUser.RoleId = user.RoleId;
            objUser.UserName = user.UserName;
            objUser.IsActive = user.IsActive;
            if (!string.IsNullOrEmpty(user.Password))
            {
                objUser.Password = Crypto.SHA512Enc(user.Password);
            }
            objUser.IsActive = user.IsActive;
            if (user.Id <= 0)
            {
                _context.User.Add(objUser);
            }

            _context.SaveChanges();

            return user;
        }

        public bool DeleteUser(UserDTO user, int? LoggedInUser)
        {
            User objUser = _context.User.Where(col => col.Id == user.Id).FirstOrDefault();
            if (objUser != null)
            {
                objUser.IsActive = false;
                objUser.DeletedDate = DateTime.Now;
                objUser.ModifiedBy = LoggedInUser;
                objUser.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
