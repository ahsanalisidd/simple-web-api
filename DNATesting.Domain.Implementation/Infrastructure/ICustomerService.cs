﻿using DNATesting.DataLayer;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Customer;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Infrastructure
{
    public interface ICustomerService
    {
        List<GetCustomerDTO> GetAll();
        CustomerDTO Add(CustomerDTO entity);
        CustomerDTO Delete(CustomerDTO entity);
        CustomerDTO Edit(CustomerDTO entity);
        GetCustomerDTO GetSingle(int id);
        Object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "");
        Object GetCustomerReport(CustomerReportDTO obj);
    }
}
