﻿using DNATesting.DataLayer;
using DNATesting.Domain.Model;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Infrastructure
{
    public interface IOrderTypeService
    {
        List<GetOrderTypeDTO> GetAll();
        OrderTypeDTO Add(OrderTypeDTO entity);
        OrderTypeDTO Delete(OrderTypeDTO entity);
        OrderTypeDTO Edit(OrderTypeDTO entity);
        GetOrderTypeDTO GetSingle(int id);
        Object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "");
    }
}
