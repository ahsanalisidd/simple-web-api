﻿using DNATesting.DataLayer;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Coupon;
using DNATesting.Domain.Model.package;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Infrastructure
{
    public interface ICouponService
    {
        List<GetCouponDTO> GetAll();
        CouponDTO Add(CouponDTO entity);
        CouponDTO Delete(CouponDTO entity);
        CouponDTO Edit(CouponDTO entity);
        GetCouponDTO GetSingle(int id);
        Object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "");
    }
}
