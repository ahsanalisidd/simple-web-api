﻿using DNATesting.DataLayer;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Content;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Infrastructure
{
    public interface IContentService
    {
        List<GetContentDTO> GetAll();
        ContentDTO Add(ContentDTO entity);
        ContentDTO Delete(ContentDTO entity);
        ContentDTO Edit(ContentDTO entity);
        GetContentDTO GetSingle(int id);
        Object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "");
    }
}
