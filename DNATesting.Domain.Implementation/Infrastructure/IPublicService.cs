﻿using DNATesting.DataLayer;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Order;
using DNATesting.Domain.Model.Public;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Infrastructure
{
    public interface IPublicService
    {
        SignUpDTO SignUp(SignUpDTO obj);
        Object SignIn(SignInDTO obj);
        List<PubPackageDTO> GetPubPackage();
        GetOrderDTO GetSingleOrder(int id);
        List<GetOrderDTO> GetAllOrder();
        GetOrderDTO CancelOrder(int id);
        GetOrderDTO CreateOrder(OrderDTO entity);
        SignUpDTO VerifyAccount(AccountVerifyDTO obj);
        Object GetPubPackageByPage(int pageNo, int noOfRecords = 10, string searchText = "");
        bool ForgotPassword(string email);
        bool UpdatePassword(ForgotPasswordDTO obj);
        bool VerificationByEmail(string email);
        PubCustomerDTO EditCustomer(PubCustomerDTO entity);
    }
}
