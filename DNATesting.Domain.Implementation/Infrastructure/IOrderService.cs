﻿using DNATesting.DataLayer;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Order;
using DNATesting.Domain.Model.package;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Infrastructure
{
    public interface IOrderService
    {
        List<GetOrderDTO> GetAll();
        OrderDTO Add(OrderDTO entity);
        OrderDTO Delete(OrderDTO entity);
        OrderDTO Edit(OrderDTO entity);
        GetOrderDTO GetSingle(int id);
        Object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "");
        Object GetOrderReport(OrderReportDTO obj);
    }
}
