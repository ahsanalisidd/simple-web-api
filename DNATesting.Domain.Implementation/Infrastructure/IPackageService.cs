﻿using DNATesting.DataLayer;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.package;
using DNATesting.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Implementation.Infrastructure
{
    public interface IPackageService
    {
        List<GetPackageDTO> GetAll();
        PackageDTO Add(PackageDTO entity);
        PackageDTO Delete(PackageDTO entity);
        PackageDTO Edit(PackageDTO entity);
        GetPackageDTO GetSingle(int id);
        Object GetPackage(int pageNo, int noOfRecords = 10, string searchText = "");
    }
}
