﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer.DBContext;
using DNATesting.DataLayer.Models;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Customer;
using DNATesting.Helper;
using DNATesting.Repository.Implementation;
using DNATesting.Repository.Infrastructure;

namespace DNATesting.Domain.Implementation
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _context;
        public CustomerService()
        {
            _context = new CustomerRepository();
        }

        public CustomerDTO Add(CustomerDTO entity)
        {
            var found = _context.FindBy(x => x.Email == entity.Email).FirstOrDefault();
            if (found == null)
            {
                var res = AutoMapper.Mapper.Map(entity, new Customer());
                res.Code = GetCode();
                res.CreatedOn = DateTime.UtcNow;
                res.CreatedBy = entity.UserId;
                res.IsTemporaryLock = false;
                res.Password = Crypto.SHA512Enc(Crypto.CreatePassword(9));
                var result = _context.Add(res);
                _context.Save();
                return AutoMapper.Mapper.Map(result, new CustomerDTO());
            }
            else
            {
                throw new Exception("Email Already exists");
            }
        }

        public CustomerDTO Delete(CustomerDTO entity)
        {
            var res = _context.Find(x => x.Id == entity.Id).FirstOrDefault();
            if (res != null)
            {
                res.DeletedDate = DateTime.UtcNow;
                _context.SoftDelete(res);
                _context.Save();
                return entity;
            }
            else
            {
                return entity;
            }
        }

        public CustomerDTO Edit(CustomerDTO entity)
        {
            //var res = AutoMapper.Mapper.Map(entity, new Customer());
            var exist = _context.Find(x => x.Email == entity.Email);

            var z = exist.ToList();
            if ((exist.ToList().Count == 1 && exist.ToList()[0].Id == entity.Id) || exist.ToList().Count == 0)
            {
                if (z.Count > 0)
                    _context.Detach(exist.FirstOrDefault());
                var found = _context.FindBy(x => x.Id == entity.Id).FirstOrDefault();
                found.Code = entity.Code;
                found.FirstName = entity.FirstName;
                found.LastName = entity.LastName;
                found.MiddleName = entity.MiddleName;
                found.Email = entity.Email;
                found.Contact = entity.Contact;
                found.Landline = entity.Landline;
                found.DOB = entity.DOB;
                found.Gender = entity.Gender;
                found.IsActive = entity.IsActive;
                found.Country = entity.Country;
                found.State = entity.State;
                found.City = entity.City;
                found.AddressLine1 = entity.AddressLine1;
                found.AddressLine2 = entity.AddressLine2;
                found.ModifiedOn = DateTime.UtcNow;
                found.ModifiedBy = entity.UserId;
                _context.Detach(found);
                _context.Edit(found);
                _context.Save();
                return AutoMapper.Mapper.Map(_context.Find(x => x.Id == entity.Id).FirstOrDefault(), new CustomerDTO());
            }
            else
            {
                throw new Exception("Email Already exists");
            }
        }


        public List<GetCustomerDTO> GetAll()
        {
            List<GetCustomerDTO> obj = new List<GetCustomerDTO>();
            var list = _context.GetAll();
            return AutoMapper.Mapper.Map(list, obj);
        }

        public GetCustomerDTO GetSingle(int id)
        {
            GetCustomerDTO obj = new GetCustomerDTO();
            var list = _context.Find(x => x.Id == id).FirstOrDefault();
            return AutoMapper.Mapper.Map(list, obj);
        }

        public object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _context.Find(col => col.DeletedDate == null);

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.FirstName.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderByDescending(col => col.CreatedOn);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<Customer> response = asQueryable.ToList();
            var mapData = AutoMapper.Mapper.Map(response, new List<GetCustomerDTO>());
            return new { TotalCount = totalRecordsCount, Data = mapData };
        }

        public string GetCode()
        {
            string id = Convert.ToString(_context.MaxId() + 1);
            string code = id.PadLeft(5, '0');
            return code;
        }

        public Object GetCustomerReport(CustomerReportDTO obj)
        {
            var result = _context.Find(x => x.CreatedOn >= obj.FromDate && x.CreatedOn <= obj.ToDate);
            if (!string.IsNullOrEmpty(obj.Gender))
                result = result.Where(x => x.Gender == obj.Gender);
            if (!string.IsNullOrEmpty(obj.Country))
                result = result.Where(x => x.Country == obj.Country);
            if (!string.IsNullOrEmpty(obj.State))
                result = result.Where(x => x.State == obj.State);
            if (!string.IsNullOrEmpty(obj.City))
                result = result.Where(x => x.City == obj.City);

            IList<Customer> response = result.ToList();
            var mapData = AutoMapper.Mapper.Map(response, new List<GetCustomerDTO>());

            return new { TotalCount = mapData.Count, Data = mapData };
        }
    }
}
