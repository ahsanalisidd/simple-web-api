﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.Domain.Model;
using DNATesting.Repository.Implementation;
using DNATesting.Repository.Infrastructure;

namespace DNATesting.Domain.Implementation
{
    public class PackageTypeService : IPackageTypeService
    {
        private readonly IPackageTypeRepository _context;
        public PackageTypeService()
        {
            _context = new PackageTypeRepository();
        }

        public PackageTypeDTO Add(PackageTypeDTO entity)
        {
            var found = _context.FindBy(x => x.Code == entity.Code).FirstOrDefault();
            if (found == null)
            {
                var result = _context.Add(AutoMapper.Mapper.Map(entity, new PackageType()));
                result.CreatedBy = entity.UserId;
                result.CreatedOn = DateTime.UtcNow;
                _context.Save();
                return AutoMapper.Mapper.Map(result, new PackageTypeDTO());
            }
            else
            {
                throw new Exception("Code Already Exist");
            }
        }

        public PackageTypeDTO Delete(PackageTypeDTO entity)
        { 
            var res = _context.Find(x => x.Id == entity.Id).FirstOrDefault();
            if (res != null)
            {
                res.DeletedDate = DateTime.UtcNow;
                _context.SoftDelete(res);
                _context.Save();
                return entity;
            }
            else
            {
                return entity;
            }
        }

        public PackageTypeDTO Edit(PackageTypeDTO entity)
        {
            var found = _context.FindBy(x => x.Code == entity.Code && x.Id != entity.Id).FirstOrDefault();
            if (found == null)
            {
                var res = AutoMapper.Mapper.Map(entity, _context.FindBy(x => x.Id == entity.Id).FirstOrDefault());
                res.ModifiedBy = entity.UserId;
                res.ModifiedOn = DateTime.UtcNow;
                _context.Edit(res);
                _context.Save();
                return AutoMapper.Mapper.Map(_context.Find(x => x.Id == entity.Id).FirstOrDefault(), new PackageTypeDTO());
            }
            else
            {
                throw new Exception("Code Already Exist");
            }
        }

        public List<GetPackageTypeDTO> GetAll()
        {
            List<GetPackageTypeDTO> obj = new List<GetPackageTypeDTO>();
            var list = _context.GetAll();
            return AutoMapper.Mapper.Map(list, obj);

        }

        public GetPackageTypeDTO GetSingle(int id)
        {
            GetPackageTypeDTO obj = new GetPackageTypeDTO();
            var list = _context.Find(x=>x.Id == id).FirstOrDefault();
            return AutoMapper.Mapper.Map(list, obj);

        }

        public object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _context.Find(col => col.DeletedDate == null);

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.Name.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderByDescending(col => col.CreatedOn);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<PackageType> response = asQueryable.ToList();
            var mapData = AutoMapper.Mapper.Map(response, new List<GetPackageTypeDTO>());
            return new { TotalCount = totalRecordsCount, Data = mapData };
        }

    }
}
