﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Domain.Implementation.Infrastructure;
using DNATesting.Domain.Model;
using DNATesting.Domain.Model.Content;
using DNATesting.Repository.Implementation;
using DNATesting.Repository.Infrastructure;

namespace DNATesting.Domain.Implementation
{
    public class ContentService : IContentService
    {
        private readonly IContentRepository _context;
        public ContentService()
        {
            _context = new ContentRepository();
        }

        public ContentDTO Add(ContentDTO entity)
        {
            var res = AutoMapper.Mapper.Map(entity, new Content());
            res.CreatedBy = entity.UserId;
            res.CreatedOn = DateTime.Now;
            var result = _context.Add(res);
            _context.Save();
            return AutoMapper.Mapper.Map( result,new ContentDTO());
        }

        public ContentDTO Delete(ContentDTO entity)
        { 
            var res = _context.Find(x => x.Id == entity.Id).FirstOrDefault();
            if (res != null)
            {
                res.DeletedDate = DateTime.UtcNow;
                _context.SoftDelete(res);
                _context.Save();
                return entity;
            }
            else
            {
                return entity;
            }
        }

        public ContentDTO Edit(ContentDTO entity)
        {
            var res = AutoMapper.Mapper.Map(entity, _context.FindBy(x=>x.Id == entity.Id).FirstOrDefault());
            res.ModifiedBy = entity.UserId;
            res.ModifiedOn = DateTime.UtcNow;
            _context.Edit(res);
            _context.Save();
            return AutoMapper.Mapper.Map(_context.Find(x=>x.Id == entity.Id).FirstOrDefault(), new ContentDTO());
        }

        public List<GetContentDTO> GetAll()
        {
            List<GetContentDTO> obj = new List<GetContentDTO>();
            var list = _context.GetAll();
            return AutoMapper.Mapper.Map(list, obj);

        }

        public GetContentDTO GetSingle(int id)
        {
            GetContentDTO obj = new GetContentDTO();
            var list = _context.Find(x=>x.Id == id).FirstOrDefault();
            return AutoMapper.Mapper.Map(list, obj);
        }

        public object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _context.Find(col => col.DeletedDate == null);

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.PageName.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderByDescending(col => col.CreatedOn);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<Content> response = asQueryable.ToList();
            var mapData = AutoMapper.Mapper.Map(response, new List<GetContentDTO>());
            return new { TotalCount = totalRecordsCount, Data = mapData };
        }

    }
}
