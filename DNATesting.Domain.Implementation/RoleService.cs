﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Domain.Model;

namespace DNATesting.Domain.Implementation
{
    public class RoleService
    {
        private readonly DNATestingContext _context;
        public RoleService()
        {
            _context = new DNATestingContext();
        }

        public Object GetRoles(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _context.Role.Where(col => col.DeletedDate == null).Include(x=>x.RoleRights).AsQueryable();

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.Name.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderByDescending(col => col.CreatedOn);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<RoleDTO> response = asQueryable.AsEnumerable().Select(col => new RoleDTO
            {
                Id = col.Id,
                Name = col.Name,
                IsActive = col.IsActive,
                CreatedOn = col.CreatedOn,
                ModifiedOn = col.ModifiedOn,
                RightIds = col.RoleRights.Where(x=>x.DeletedDate == null).Select(r => r.RightId).ToList(),
                Rights = col.RoleRights.Where(x => x.DeletedDate == null).Select(r => new RightDTO
                {
                    Id = r.Right.Id,
                    Name = r.Right.Name
                }).ToList(),
                CreatedBy = col.CreatedBy,
                ModifiedBy = col.ModifiedBy
            }).ToList();


            return new { TotalCount = totalRecordsCount, Data = response };
        }
        public RoleDTO GetRole(int roleId)
        {
            var asQueryable = _context.Role.Where(col => col.IsActive == true && col.Id == roleId).AsQueryable();

            RoleDTO response = asQueryable.AsEnumerable().Select(col => new RoleDTO
            {
                Id = col.Id,
                Name = col.Name,
                IsActive = col.IsActive,
                CreatedOn = col.CreatedOn,
                ModifiedOn = col.ModifiedOn,
                RightIds = col.RoleRights.Where(x => x.DeletedDate == null).Select(r => r.RightId).ToList(),
                Rights = col.RoleRights.Where(x => x.DeletedDate == null).Select(r => new RightDTO
                {
                    Id = r.Right.Id,
                    Name = r.Right.Name
                }).ToList(),
                CreatedBy = col.CreatedBy,
                ModifiedBy = col.ModifiedBy
            }).FirstOrDefault();


            return response;
        }

        public RoleDTO SaveRole(RoleDTO role, int? LoggedInUser)
        {
            Role objRole;
            if (role.Id > 0)
            {
                objRole = _context.Role.Where(col => col.Id == role.Id).FirstOrDefault();
                if (objRole == null)
                    return null;

                objRole.ModifiedOn = DateTime.Now;
                objRole.ModifiedBy = LoggedInUser;
            }
            else
            {
                objRole = new Role();
                objRole.CreatedOn = DateTime.Now;
                objRole.CreatedBy = LoggedInUser;
            }
            objRole.Name = role.Name;
            objRole.IsActive = true;

            if (role.Id <= 0)
                _context.Role.Add(objRole);
            _context.SaveChanges();

            List<RoleRight> existingRoleRights = _context.RoleRight.Where(col => col.RoleId == objRole.Id).ToList();
            foreach (var item in existingRoleRights)
            {
                item.DeletedDate = DateTime.Now;
                item.ModifiedBy = LoggedInUser;
                item.ModifiedOn = DateTime.Now;
            }
            _context.SaveChanges();

            RoleRight objRoleRight;
            foreach (int rightId in role.RightIds)
            {
                objRoleRight = new RoleRight();
                objRoleRight.RoleId = objRole.Id;
                objRoleRight.RightId = rightId;
                objRoleRight.CreatedOn = DateTime.Now;
                objRoleRight.CreatedBy = LoggedInUser;
                _context.RoleRight.Add(objRoleRight);
            }
            _context.SaveChanges();

            role.Id = objRole.Id;
            return role;
        }

        public bool DeleteRole(RoleDTO role, int? LoggedInUser)
        {
            Role objRole = _context.Role.Where(col => col.Id == role.Id).FirstOrDefault();
            if (objRole != null)
            {
                objRole.DeletedDate = DateTime.Now;
                objRole.ModifiedBy = LoggedInUser;
                objRole.IsActive = false;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
