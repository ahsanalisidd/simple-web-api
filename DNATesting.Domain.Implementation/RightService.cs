﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNATesting.DataLayer;
using DNATesting.DataLayer.DBContext;
using DNATesting.Domain.Model;

namespace DNATesting.Domain.Implementation
{
    public class RightService
    {
        private readonly DNATestingContext _context;
        public RightService()
        {
            _context = new DNATestingContext();
        }

        public Object GetRights(int pageNo, int noOfRecords = 10, string searchText = "")
        {
            var asQueryable = _context.Right.Where(col => col.IsActive == true).AsQueryable();

            if (!string.IsNullOrEmpty(searchText))
            {
                asQueryable = asQueryable.Where(col => col.Name.Contains(searchText));
            }
            var totalRecordsCount = asQueryable.Count();

            asQueryable = asQueryable.OrderBy(col => col.OrderNo);
            if (pageNo > 0)
            {
                asQueryable = asQueryable.Skip(noOfRecords * (pageNo - 1)).Take(noOfRecords);
            }

            IList<RightDTO> response = asQueryable.Select(col => new RightDTO
            {
                Id = col.Id,
                Name = col.Name,
                IsActive = col.IsActive,
                CreatedOn = col.CreatedOn
            }).ToList();


            return new { TotalCount = totalRecordsCount, Data = response };
        }
        public RightDTO GetRight(int rightId)
        {
            var asQueryable = _context.Right.Where(col => col.IsActive == true && col.Id == rightId).AsQueryable();

            RightDTO response = asQueryable.Select(col => new RightDTO
            {
                Id = col.Id,
                Name = col.Name,
                IsActive = col.IsActive,
                CreatedOn = col.CreatedOn
            }).FirstOrDefault();


            return response;
        }

        public RightDTO SaveRight(RightDTO right)
        {
            Right objRight;
            if (right.Id > 0)
            {
                objRight = _context.Right.Where(col => col.Id == right.Id).FirstOrDefault();
            }
            else
            {
                objRight = new Right();
                objRight.CreatedOn = DateTime.Now;
            }
            objRight.Name = right.Name;
            objRight.OrderNo = right.OrderNo;
            objRight.IsActive = true;

            if (right.Id <= 0)
                _context.Right.Add(objRight);
            _context.SaveChanges();

            right.Id = objRight.Id;
            return right;
        }

        public bool DeleteRight(RightDTO right)
        {
            Right objRight = _context.Right.Where(col => col.Id == right.Id).FirstOrDefault();
            if (objRight != null)
            {
                objRight.IsActive = false;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
