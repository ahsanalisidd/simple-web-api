﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.Public
{
    public class ForgotPasswordDTO
    {
        public string VerificationCode { get; set; }
        public string NewPassword { get; set; }
    }
}
