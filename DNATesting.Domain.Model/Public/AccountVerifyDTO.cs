﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.Public
{
    public class AccountVerifyDTO
    {
        public string VerificationCode { get; set; }
    }
}
