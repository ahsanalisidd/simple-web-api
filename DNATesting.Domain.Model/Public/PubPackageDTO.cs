﻿using DNATesting.Domain.Model.package;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.Public
{
    public class PubPackageDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public IList<PackageDetailDTO> PackageDetailDTOs { get; set; }

        public PubPackageDTO()
        {
            PackageDetailDTOs = new List<PackageDetailDTO>();
        }
    }
}
