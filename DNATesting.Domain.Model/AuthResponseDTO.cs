﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model
{
    public class AuthResponseDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string ProfilePicture { get; set; }
        public string AccessToken { get; set; }
        public List<string> Rights { get; set; }
    }
}
