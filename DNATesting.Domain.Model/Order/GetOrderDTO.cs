﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.Order
{
    public class GetOrderDTO : GeneralDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime? OrderDate { get; set; }
        public string Description { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string CurrentStatus { get; set; }
        public int OrderRecievingType { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public decimal? TotalAmount { get; set; }
        public int? CouponId { get; set; }
        public decimal? DiscountOnCoupon { get; set; }
        public decimal? NetAmount { get; set; }
        public string PaymentMode { get; set; }
        public virtual ICollection<OrderDetailDTO> OrderDetailsDTO { get; set; }
    }
}
