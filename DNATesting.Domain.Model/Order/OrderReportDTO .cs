﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.Order
{
    public class OrderReportDTO
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}
