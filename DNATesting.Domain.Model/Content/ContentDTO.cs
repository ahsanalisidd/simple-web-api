﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.Content
{
    public class ContentDTO
    {
        public int Id { get; set; }
        public string PageTitle { get; set; }
        public string PageName { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
