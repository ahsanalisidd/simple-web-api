﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model
{
    public class RoleRightDTO : GeneralDTO
    {
        public int Id { get; set; }
        [Required]
        public Nullable<int> RoleId { get; set; }
        [Required]
        public Nullable<int> RightId { get; set; }
    }
}
