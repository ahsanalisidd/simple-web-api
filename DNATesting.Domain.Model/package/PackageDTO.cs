﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.package
{
    public class PackageDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> PackageTypeId { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> NetAmount { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int UserId { get; set; }
        public IList<PackageDetailDTO> PackageDetailDTOs { get; set; }

        public PackageDTO()
        {
            PackageDetailDTOs = new List<PackageDetailDTO>();
        }
    }
}
