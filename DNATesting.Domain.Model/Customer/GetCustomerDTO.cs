﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model.Customer
{
    public class GetCustomerDTO: GeneralDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Landline { get; set; }
        public DateTime? DOB { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public Nullable<bool> IsAccountActive { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
    }
}
