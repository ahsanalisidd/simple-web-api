﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model
{
    public class GetByPageDTO
    {
        public int NoOFPage { get; set; }

        public int NoOfRecords { get; set; }

        public string SearchText { get; set; }
    }
}
