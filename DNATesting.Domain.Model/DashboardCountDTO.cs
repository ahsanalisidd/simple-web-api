﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNATesting.Domain.Model
{
    public class DashboardCountDTO
    {
        public int? UserCount { get; set; }
        public int? RoleCount { get; set; }
        public int? Draft { get; set; }
        public int? InProgress { get; set; }
        public int? Approve { get; set; }
        public int? Cancelled { get; set; }
        public int? MaleCustomers { get; set; }
        public int? FemaleCustomers { get; set; }
        public int? NotSpecifiedCustomers { get; set; }
        public int? Packages { get; set; }
    }
}
